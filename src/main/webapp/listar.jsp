<%@page import="java.util.Iterator"%>
<%@page import="java.util.List"%>
<%@page import="solemnedos.entity.Persona"%>
<%@page import="solemnedos.entity.Persona"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%
    List<Persona> persona = (List<Persona>) request.getAttribute("persona");
    Iterator<Persona> itPersona = persona.iterator();
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Lista de Personas</title>
    </head>
    <body>
        <form name="frmListaPersona" action="ListarControlador" method="Post">
            <%if (persona != null) {%>
            <table border="1" cellpadding="3" cellspacing="0" width="650" align="center">
                <tr>
                    <td colspan="6" align="center"><h2>Listado de Personas :</h2></td>
                </tr>
                <tr bgcolor="#ababab" align="center">
                    <td><b>Nombre</b></td>
                    <td><b>Apellido</b></td>
                    <td><b>Rut</b></td>
                    <td><b>Edad</b></td>
                    <td><b>Sueldo</b></td>
                    <td><b>Accion</b></td>
                </tr>
                <%while (itPersona.hasNext()) {
                        Persona personas = itPersona.next();%>
                <tr>
                    <td><%= personas.getNombre()%></td>
                    <td><%= personas.getApellido()%></td>
                    <td><%= personas.getRut()%></td>
                    <td><%= personas.getEdad()%></td>
                    <td><%= personas.getSueldo()%></td>
                    <td><input type="radio" name="seleccion" value="<%= personas.getRut()%>"></td>
                </tr>
                <%}%>
                <tr>
                    <td colspan="3" align="center"><input type="submit" name="accion" value="Editar"></td>
                    <td colspan="3" align="center"><input type="submit" name="accion" value="Eliminar"></td>
                </tr>
            </table>
            <%}%>
        </form>
    </body>
</html>

































