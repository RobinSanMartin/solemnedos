<%@page import="solemnedos.entity.Persona"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%
    Persona persona=(Persona) request.getAttribute("persona");
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Editar Persona</title>
    </head>
    <body>
        <form name="Registro" action="ListarControlador" method="POST">
            <table border="0">
                <h1>EDITAR PERSONA</h1>
                <tr>
                    <td>Nombre:</td>
                    <td><input type="text" name="txtNombre" value="<%= persona.getNombre()%>"></td>
                </tr>
                <tr>
                    <td>Apellido:</td>
                    <td><input type="text" name="txtApellido" value="<%= persona.getApellido()%>"></td>
                </tr>
                <tr>
                    <td>Rut:</td>
                    <td><input type="text" name="txtRut" value="<%= persona.getRut()%>" readonly></td>
                </tr>
                <tr>
                    <td>Edad:</td>
                    <td><input type="text" name="txtEdad" value="<%= persona.getEdad()%>"></td>
                </tr>
                <tr>
                    <td>Sueldo:</td>
                    <td><input type="text" name="txtSueldo" value="<%= persona.getSueldo()%>"></td>
                </tr>
                <tr><td>&nbsp;</td></tr>
                <tr>
                    <td><input type="submit" name="accion" value="Grabar"></td>
                    <td><input type="submit" name="accion" value="Cancelar"></td>
                </tr>  

            </table>
        </form>
    </body>
</html>
