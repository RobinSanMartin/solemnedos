/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import solemnedos.dao.PersonaJpaController;
import solemnedos.dao.exceptions.NonexistentEntityException;
import solemnedos.entity.Persona;

/**
 *
 * @author XSefe
 */
@WebServlet(name = "ListarControlador", urlPatterns = {"/ListarControlador"})
public class ListarControlador extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ListarControlador</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet ListarControlador at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        PersonaJpaController dao = new PersonaJpaController();
        List<Persona> persona = dao.findPersonaEntities();
        request.setAttribute("persona", persona);
        request.getRequestDispatcher("listar.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String boton = request.getParameter("accion");
        if(boton.equals("Cancelar")){
            request.getRequestDispatcher("listar.jsp").forward(request, response);
        }
        if (boton.equals("Grabar")) {
            try {
                String nombre = request.getParameter("txtNombre");
                String apellido = request.getParameter("txtApellido");
                String rut = request.getParameter("txtRut");
                int edad = Integer.parseInt(request.getParameter("txtEdad"));
                int sueldo = Integer.parseInt(request.getParameter("txtSueldo"));
                Persona personas= new Persona();
                personas.setNombre(nombre);
                personas.setApellido(apellido);
                personas.setRut(rut);
                personas.setEdad(edad);
                personas.setSueldo(sueldo);
                PersonaJpaController dao = new PersonaJpaController();
                dao.edit(personas);
                List<Persona> persona = dao.findPersonaEntities();
                request.setAttribute("persona", persona);
                request.getRequestDispatcher("listar.jsp").forward(request, response);
            } catch (Exception ex) {
                Logger.getLogger(ListarControlador.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        if (boton.equals("Eliminar")) {
            try {
                String rut = request.getParameter("seleccion");
                PersonaJpaController dao = new PersonaJpaController();
                dao.destroy(rut);
                List<Persona> persona = dao.findPersonaEntities();
                request.setAttribute("persona", persona);
                request.getRequestDispatcher("listar.jsp").forward(request, response);
            } catch (NonexistentEntityException ex) {
                Logger.getLogger(ListarControlador.class.getName()).log(Level.SEVERE, null, ex);
            }
        }else if(boton.equals("Editar")){
            String rut = request.getParameter("seleccion");
            PersonaJpaController dao = new PersonaJpaController();
            Persona persona=dao.findPersona(rut);
            request.setAttribute("persona", persona);
            request.getRequestDispatcher("editar.jsp").forward(request, response);
        }
    }
    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
